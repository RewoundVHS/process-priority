.TL
Homework 2
.AU
Neve Laughery
.AI
Edinboro University of Pennsylvania
.nr step 1 1
.IP \n[step]
Read chapters 2 and 3 of the book
.IP \n+[step]
Pick any one of the following unix utilities (awk, sed, grep, groff, diff, sort,
bc), preferably one you are not familiar with. You may look at either unix or
gnu versions of a utility. (awk or gawk for example)
.RS
.IP \[bu]
I chose groff as I have used LaTeX to typeset in the past and this utility seems
like a more minimalist solution. According to GNU.org Groff is a typesetting
system that reads plain text and formatting commands and produces formatted
output.
.IP \[bu]
Groff is used to produce man pages and other documents. Groff can compile these
documents with incredible speed compared to other more "fully featured"
typesetting systems such as LaTeX.
.IP \[bu]
I wrote this document using groff. The source code is available at:
.PP
https://GitLab.com/RewoundVHS/process-priority
.PP
To compile this document in pdf format use the following:
.PP
groff -ms homework2.ms -T pdf > homework2.pdf
.RE
