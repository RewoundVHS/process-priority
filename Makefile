CXXFLAGS = -g -O3 -Wall -Wextra -Wuninitialized -pedantic -Wshadow -Weffc++ -std=c++14

OBJS = proc-prio

all: ${OBJS}

clean:
	rm -f proc-prio *.o
