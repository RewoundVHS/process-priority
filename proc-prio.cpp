#include <iostream>
#include <unistd.h>
#include <sys/resource.h>
#include <errno.h>
#include <cstring>
#include <cerrno>

using namespace std;

int main() {
	pid_t pid;
	int prio;

	// Get this process' pid
	pid = getpid();

	// Get this process' priority
	cout << "Attempting to get priority for this process with pid " << pid 
		<< "." << endl;
	prio = getpriority(PRIO_PROCESS, pid);
	cout << "The priority for process " << pid << " is " << prio << "." 
		<< endl << endl;

	// Set this process' priority to 10
	cout << "Attempting to set priority for this process with pid " << pid 
		<< " to 10." << endl;
	setpriority(PRIO_PROCESS, pid, 10);
	prio = getpriority(PRIO_PROCESS, pid);
	cout << "The priority for process " << pid << " is now " << prio << "." 
		<< endl << endl;

	// Attempt to change priority to 1
	cout << "Attempting to set priority for this process with pid " << pid 
		<< " to 1." << endl;
	setpriority(PRIO_PROCESS, pid, 1);
	prio = getpriority(PRIO_PROCESS, pid);
	cout << "The priority for process " << pid << " is now " << prio << "." 
		<< endl;
	cerr << "\tThe error number is " << errno << endl;
	cerr << "\tAnd the error string is " << strerror(errno) << endl << endl;

	// Attempt to get priority for nonexistent pid -1
	cout << "Attempting to get priority for process -1." << endl;
	prio = getpriority(PRIO_PROCESS, -1);
	cout << "The priority for process -1 is " << prio << "." << endl;
	cerr << "\tThe error number is " << errno << endl;
	cerr << "\tAnd the error string is " << strerror(errno) << endl << endl;

	// Get the priority for pid 1, the init system
	cout << "Attempting to get priority for process 1." << endl;
	prio = getpriority(PRIO_PROCESS, 1);
	cout << "The priority for process 1 is " << prio << "." << endl;
}
